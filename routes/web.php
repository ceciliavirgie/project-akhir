<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {  
    Route::get('/' , 'DashboardController@index' );
    Route::resource('jawab', 'JawabController');
    Route::resource('kategori', 'KategoriController');
    Route::resource('tanya', 'TanyaController');

    Route::resource('profile','ProfileController')->only([
        'index','update','create'
    ]);
    Route::resource('dashboard','DashboardController')->only([
        'index'
    ]);

    Route::get('tanya-export', 'DownloadController@export');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

