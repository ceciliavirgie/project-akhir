<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Focus - Bootstrap Admin Dashboard </title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('admin/images/favicon.png')}}">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Sign up your account</h4>
                                    <form method="POST" action="{{ route('register') }}" >
                                        @csrf
                                        <div class="form-group">
                                            <label for="username"><strong>Username</strong></label>
                                            <input type="text" class="form-control" name="name" placeholder="Masukkan username"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="nama"><strong>Nama</strong></label>
                                            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="email"><strong>{{ __('E-Mail Address') }}</strong></label>
                                            <input id="email" type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="hello@example.com" required autocomplete="email" autofocus>

                                            @error('email')
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label><strong>{{ __('Password') }}</strong></label>
                                            <input id="password" type="password" class="form-control " name="password" required autocomplete="new-password" placeholder="Masukkan password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label><strong>{{ __('Confirm Password') }}</strong></label>
                                            <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" placeholder="Masukkan kembali password">
                                        </div>

                                        <div class="form-group">
                                            <label for="umur"><strong>{{ __('Umur') }}</strong></label>
                                            <input id="email" type="number" class="form-control" name="umur" value="{{ old('umur') }}" required autocomplete="umur" autofocus placeholder="Masukkan umur">

                                            @error('umur')
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="bio"><strong>{{ __('Bio') }}</strong></label>
                                            <input id="bio" type="text" class="form-control  @error('bio') is-invalid @enderror" name="bio" value="{{ old('bio') }}" required autocomplete="bio" autofocus placeholder="Masukkan bio">

                                            @error('bio')
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat"><strong>{{ __('Alamat') }}</strong></label>
                                            <input id="alamat" type="text" class="form-control  @error('bio') is-invalid @enderror" name="alamat" value="{{ old('bio') }}" required autocomplete="bio" autofocus placeholder="Masukkan alamat">

                                            @error('alamat')
                                             <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary btn-block"> {{ __('Register') }}</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p>Already have an account? <a class="text-primary" href="/login">Sign in</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('admin/vendor/global/global.min.js')}}"></script>
    <script src="{{asset('admin/js/quixnav-init.js')}}"></script>
    <!--endRemoveIf(production)-->
</body>

</html>