@extends('layouts.master')
@section('content')
<div>
    <a href="/tanya/create" class="btn btn-success btn-sm mb-3 ml- text-white"><i class="fa fa-plus" aria-hidden="true"></i>  Tambah</a>
    <div class="row">
    @forelse ($detail->pertanyaan as $item)
    <div class = "col-4 mb-2 mr-4">
    <div class="card" style="width:10srem;">
    <div class="card">
    <div class="card-header bg-primary">
      <h3 class="card-title text-white mb-2">{{ $detail->nama }}</h3>
    </div>
    <div class="card-body">
      <p class="card-text text-bold">{!!$item->pertanyaan!!}</p>
          <form  action="/kategori" method="POST">
              <a href="/tanya/{{$item->id}}" class="btn btn-primary btn-sm text-white mr-2">Show</a>
              <a href="/tanya/{{ $item->id }}/edit" class="btn btn-success btn-sm text-white  mr-2">Edit</a>
              @csrf
              @method('DELETE')
              <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
          </form>
    </div>
  </div>
    @empty
        <h1>Tidak ada pertanyaan di dalam kategori ini</h1>
    @endforelse
    </div>
</div>
@endsection