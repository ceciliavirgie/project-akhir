@extends('layouts.master')
@section('content')
<div class="mt-2 ml-3">
    <div class="card">
        <div class="card-header bg-primary">
          <h3 class="card-title text-white mb-2">Tabel Kategori</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if (session('success'))
              <div class="text-white alert alert-success">
                {{session('success')}}
              </div>
          @endif
          <a href="/kategori/create" class="btn btn-success btn-sm mb-3 text-white"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
          <table class="table table-bordered border-dark" >
            <thead>
              <tr style="border: 1px solid text-dark">
                <th style="width: 8px" >No</th>
                <th>Judul</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              @forelse($posts as $key => $kategori)
                <tr>
                    <td> {{ $key +1 }}</td>
                    <td> {{ $kategori->nama }}</td>
                    <td> 
                      <form  action="/kategori/{{$kategori->id}}" method="POST">
                        <a href="/kategori/{{$kategori->id}}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/kategori/{{ $kategori->id }}/edit" class="btn btn-success btn-sm text-white">Edit</a>
                          @csrf
                          @method('delete')
                          <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                      </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="4" allign='center'>Tidak Ada Daftar Kategori</td>
                    </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> 
      </div>
</div>
@endsection