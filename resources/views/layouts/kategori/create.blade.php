@extends('layouts.master')
@section('content')
<div class="card card-primary">
    <!-- form start -->
    <form action="/kategori" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group ">
          <label for="nama" class="text-black"><b> Nama Kategori</b></label>
          <input type="text" class="form-control text-body" name="nama" placeholder="Masukkan kategori">

          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div> 
        <button type="submit" class="btn btn-primary">Tambahkan Kategori</button>   
      </div>
    </form>
  </div>
@endsection