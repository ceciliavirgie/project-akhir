@extends('layouts.master')
@section('content')
  <div class="card">
    <div class="card-header bg-primary">
      <h3 class="card-title text-white mb-2">Detail Pertanyaan</h3>
    </div>
    <div class="card-body">
      <p>{!! $temp->pertanyaan !!}</p>
    </div>
  </div>


  @auth 
    <div class="card">
      <div class="card-header bg-primary">
        <h3 class="card-title text-white mb-2">Jawaban</h3>
      </div>
      <div class="card-body">
      @forelse ($temp->jawab as $jawaban)
        <div class="card bg-light mb-3" style="background-color:$indigo-100">
          <div class="card-header">
            <h5>{{$jawaban->user->name}}</h5>
            <ul class="nav justify-content-end ml-3">
              <li class="nav-item">
                <a class="btn btn-success text-white ml-2 mr-2" href="/jawab/{{$jawaban->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
              </li>
              <li class="nav-item">
                <form action="/jawab/{{$jawaban->id}}" method="post">
                  @csrf 
                  @method('DELETE')
                  <input type="submit" value="Delete" class="btn btn-danger">
                </form>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div>
              <h6>{!! $jawaban->jawaban !!}</h6>
            </div>
          </div>
        </div>
        @empty
          <td colspan="4" allign='center'>Tidak Ada Jawaban</td>
      @endforelse

      <form action="/jawab" method="POST">
          @csrf 
          <input type="hidden" name="temp_id" value="{{$temp->id}}" id="">
          <textarea name="jawaban" class="form-control" cols="30" rows="10" placeholder="Masukkan Jawaban"></textarea>
          <input type="submit" class="btn btn-info mt-3 text-white" value="Tambahkan Jawaban">          
      </form>
    </div> 
  @endauth

  @push('script')
    <script src="https://cdn.tiny.cloud/1/m7i1r3xa1hpbavjaigab3lr2dh6qvtqoo6ew0qgjkyb0fe6s/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
    tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    });
  </script>
  @endpush

@endsection