@extends('layouts.master')

@section('content')
<div class="card">
  <div class="card-header bg-primary">
    <h3 class="card-title text-white mb-2">Tabel Daftar Pertanyaan</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if(session('success'))
      <div class="alert alert-success">
        {{ session('success')}}
      </div>
    @endif
      <a class="btn btn-success text-white" href="/tanya/create"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
    <table class="table table-bordered table-striped mt-3">
      <thead>                  
        <tr class="text-center">
          <th>Pertanyaan</th>
          <th style="width: 300px">Informasi</th>
          <th style="width: 100px">Hapus</th>
        </tr>
      </thead>
        <tbody>
          @forelse($temp as $tanya)
            <tr>
                <td>
                  {!! $tanya->pertanyaan !!}
                </td>
                <td style="display:flex">
                  <a href="/tanya/{{$tanya->id}}" class="btn btn-primary text-white ml-2 mr-2">Lihat Jawaban</a>
                  <a href="/tanya/{{ $tanya->id }}/edit" class="btn btn-success text-white ml-2 mr-2">Edit Pertanyaan</a>
                </td>
                <td class="text-center">
                  <form action="/tanya/{{$tanya->id}}" method="post">
                    @csrf 
                    @method('DELETE')
                    <input type="submit" value="Hapus" class="btn btn-danger">                
                  </form>
                </td>
            </tr>
             @empty
            <tr>
            <td colspan="4" allign='center'>Tidak Ada Pertanyaan</td>
            </tr>
          @endforelse
        </tbody>
    </table>
  </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
      <ul class="pagination pagination-sm m-0 float-right">
        <li class="page-item"><a class="page-link" href="#">«</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">»</a></li>
      </ul>
  </div>
</div>
@endsection

