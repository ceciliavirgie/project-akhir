@extends('layouts.master')
@section('content')
<div class="mt-0">
    <div class="card card-primary ">
        <div class="card-header bg-primary">
          <h3 class="card-title text-white mb-2">Edit Pertanyaan</h3>
        </div>
       
        <!-- form start -->
        <form role="form" action="/tanya/{{$tanya->id}}" method="POST">
          @csrf
          @method('put')
            <div class="card-body">
                <div class="form-group ">
                <label class="text-black" for="pertanyaan">Pertanyaan</label>
                <input type="text" class="form-control" name="pertanyaan" placeholder="Masukkan perubahan pertanyaan">
        
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                </div>    
                <button type="submit" class="btn btn-success text-white">Simpan Perubahan Pertanyaan</button> 
            </div>  
        </form>
    </div>
</div>
@endsection