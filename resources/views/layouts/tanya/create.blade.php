@extends('layouts.master')
@section('content')
<div class="card card-primary">
    <!-- form start -->
    <form action="/tanya" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="pertanyaan"><b>Pertanyaan : </b></label><br><br>
          <textarea name="pertanyaan" id="" cols="150" rows="7" placeholder="Masukkan pertanyaan">
          </textarea>
          @error('pertanyaan')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div> 
        <select name="category">
              @foreach($kategories as $kat)
                  <option value="{{$kat->id}}">{{$kat->nama}}</option>
              @endforeach
          </select>
        <button type="submit" class="btn btn-primary ml-3">Tambah Pertanyaan</button>   
      </div>
    </form>
  </div>
  
  @push('script')
    <script src="https://cdn.tiny.cloud/1/m7i1r3xa1hpbavjaigab3lr2dh6qvtqoo6ew0qgjkyb0fe6s/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
      tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      });
    </script>
  @endpush
  
@endsection