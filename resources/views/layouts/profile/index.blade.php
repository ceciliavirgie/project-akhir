@extends('layouts.master')
@section('content')
<div class="col-lg-12">
    <div class="card card-primary">
        <div class="profile">
            <div class="profile-head">
                <div class="photo-content">
                    <div class="cover-photo"></div>
                    <div class="profile-photo">
                        <img src="{{asset('admin/images/profile/profile.png')}}" class="img-fluid rounded-circle" alt="">
                    </div>
                </div>
                <div class="profile-info">
                    <div class="row justify-content-center">
                        <div class="col-xl-8">
                            <div class="row">
                                <div class="col-xl-4 col-sm-4 border-right-1 prf-col">
                                    <div class="profile-name">
                                        <h4 class="text-primary">{{$profile->user->name}}</h4>
                                        <p>{{$profile->umur}}</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-sm-5 border-right-1 prf-col">
                                    <div class="profile-email">
                                        <h4 class="text-muted">{{$profile->user->email}}</h4>
                                        <p>{{$profile->alamat}}</p>
                                    </div>
                                </div>
                                <div class="justify-content-end">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#contohModalScroll">Edit Profile</button>
                                    <div class="modal fade" id="contohModalScroll" tabindex="-1" role="dialog" aria-labelledby="contohModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-scrollable" role="document"> -->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="contohModalScrollableTitle">Edit Profile</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form role="form" action="/profile/{{$profile->id}}" method="POST">
                                                @csrf 
                                                @method('PUT')
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="email">E-mail</label>
                                                    <input type="text" class="form-control" id="email" name="email" value="{{$profile->user->email}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Username</label>
                                                    <input type="text" class="form-control" id="name" name="name" value="{{$profile->user->name}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama">Nama</label>
                                                    <input type="text" class="form-control" id="nama" name="nama" value="{{old ('nama', $profile->nama)}}" placeholder="Masukkan Nama" required>
                                                    @error('nama')
                                                    <div class="alert alert-danger">
                                                    {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="umur">Umur</label>
                                                    <input type="number" class="form-control" id="umur" name="umur" value="{{old ('umur', $profile->umur)}}" placeholder="Masukkan Umur" required>
                                                    @error('umur')
                                                    <div class="alert alert-danger">
                                                    {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="bio">Bio</label>
                                                    <input type="text" class="form-control" id="bio" name="bio" value="{{old ('bio', $profile->bio)}}" placeholder="Masukkan Bio" required>
                                                    @error('bio')
                                                    <div class="alert alert-danger">
                                                    {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <input type="text" class="form-control" id="alamat" name="alamat" value="{{old ('alamat', $profile->alamat)}}" placeholder="Masukkan Alamat" required>
                                                    @error('alamat')
                                                    <div class="alert alert-danger">
                                                    {{$message}}
                                                    </div>
                                                    @enderror
                                                </div>
                                                </div>
                                                <!-- /.card-body -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="profile-statistics">
                        <div class="text-center mt-4 border-bottom-1 pb-3">
                            <div class="row">
                                <div class="col">
                                    <h3 class="m-b-0">10</h3><span>Follower</span>
                                </div>
                                <div class="col">
                                    <h3 class="m-b-0">{{$tanya}}</h3><span>Pertanyaan</span>
                                </div>
                                <div class="col">
                                    <h3 class="m-b-0">{{$jawab}}</h3><span>Jawaban</span>
                                </div>
                            </div>
                            <div class="mt-4"><a href="javascript:void()" class="btn btn-primary pl-5 pr-5 mr-3 mb-4">Follow</a> <a href="javascript:void()" class="btn btn-dark pl-5 pr-5 mb-4">Send
                                    Message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-bold">Tabel Data Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                  @endif
                  <a class="btn btn-primary" href="/tanya/create"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>
                  <a class="btn btn-success text-white" href="tanya-export"><i class="fa fa-download" aria-hidden="true"></i> Download</a>
                <table class="table table-bordered table-striped mt-3">
                  <thead>                  
                    <tr class="text-center">
                      <th>Pertanyaan</th>
                      <th style="width: 300px">Informasi</th>
                      <th style="width: 100px">Hapus</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($temp as $tanya)
                        <tr>
                            <td>
                                {!! $tanya->pertanyaan !!}
                            </td>
                            <td style="display:flex">
                                <a href="/tanya/{{$tanya->id}}" class="btn btn-primary text-white ml-2 mr-2">Lihat Jawaban</a>
                                <a href="/tanya/{{ $tanya->id }}/edit" class="btn btn-success text-white ml-2 mr-2">Edit Pertanyaan</a>
                            </td>
                            <td class="text-center">
                                <form action="/tanya/{{$tanya->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="submit" value="Hapus" class="btn btn-danger">
                                    
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                        <td colspan="4" allign='center'>Tidak Ada Pertanyaan</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
        </div>

        </div>
    </div>
</div>
@endsection