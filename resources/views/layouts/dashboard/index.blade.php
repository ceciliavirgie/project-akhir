@extends('layouts.master')
@section('content')
<div class="card m-0">
    <div class="card-body">
        <div class="row page-titles mb-1 ">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Halo, {{ Auth::user()->name }} Selamat datang!</h4>
                    <p class="mb-0 mt-0 m-0">Mulai bertanya dan temukan jawabannya sekarang juga!</p>
                    <a class="btn btn-info mr-3 mt-3 text-white" href="/profile"><span class="mr-2"><i class="icon icon-single-04 "></i></span>Profile</a>
                    <a class="btn btn-warning mt-3 text-white" href="/kategori"><span class="mr-2"><i class="fa fa-reply"></i></span>Kategori</a>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-0 mt-sm-0 d-flex m-0">
                <a href="/tanya" class="btn btn-primary btn-lg">Mulai bertanya</a>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-6 p-md-0 mt-3">
    <div class="card">
        <div class="card-header bg-primary">
            <h4 class="card-title text-white mb-2">Pertanyaanmu</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table student-data-table m-t-20">
                    <thead>
                        <tr>
                            <th>Pertanyaan</th>
                            <th>Kategori</th>
                            <th>Aksi</th>
                            <th class="text-center">Hapus</th>
                        </tr>
                        <!-- <tr>
                            <th>Pertanyaan</th>
                            <th>Kategori</th>
                            <th>Aksi</th>
                        </tr> -->
                    </thead>
                    <tbody>
                    @forelse($posts as $tanya)
                        <tr>
                            <td>
                            {!!$tanya->pertanyaan !!}
                            </td>
                            <td>
                                <button type="badge" class="btn btn-outline-primary">{{ $tanya->kategori->nama }}<span class="badge badge-light"></span></button>
                            </td>
                            <td>
                                <a href="/tanya/{{$tanya->id}}" class="btn btn-primary btn-sm">Lihat Detail</a>
                            </td>
                            <td class="text-center">
                                <form action="/tanya/{{$tanya->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="submit" value="Hapus" class="btn btn-danger">
                                    
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" allign='center'> Belum ada data</td>
                            </tr>
                        @endforelse
                       
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection