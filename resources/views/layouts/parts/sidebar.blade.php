<div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    @auth
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-single-04"></i><span class="nav-text">{{ Auth::user()->name }}</span></a>
                        <ul aria-expanded="false">
                            <li><a href="/profile">Profile </a></li>
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">{{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li> 
                    <li><a href="/"><i class="icon icon-world-2"></i><span class="nav-text">Dashboard</span></a>
                    </li>             
                    @endauth
                    @guest
                    <li class="nav-item"> 
                        <a class="nav-link" href="/login">
                            <i class="fas fa-fw fa-tachometer-alt"></i>
                            <span>Login</span></a>
                    </li>
                    @endguest
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-app-store"></i><span class="nav-text">Tanya - Jawab</span></a>
                        <ul aria-expanded="false">
                            <li><a href="/kategori">Kategori Pertanyaan</a></li>
                            <li><a href="/tanya">Pertanyaan</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>