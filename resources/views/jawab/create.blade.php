@extends('layouts.master')

@section('content')
    <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Jawaban :</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/jawab" method="POST">
                    @csrf 
                    <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control z-depth-1" id="jawaban" name="jawaban" value="{{old ('jawaban', '')}}" rows="3" placeholder="Masukkan jawaban"></textarea>
                        @error('jawaban')
                          <div class="alert alert-danger">
                          {{$message}}
                          </div>
                        @enderror
                    </div>                   
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                    <button type="submit" class="btn btn-primary text-white">Tambahkan Jawaban</button>
                    </div>
                </form>
                </div>

                @push('script')
                    <script src="https://cdn.tiny.cloud/1/m7i1r3xa1hpbavjaigab3lr2dh6qvtqoo6ew0qgjkyb0fe6s/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                    <script>
                        tinymce.init({
                        selector: 'textarea',
                        plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                        toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                        toolbar_mode: 'floating',
                        tinycomments_mode: 'embedded',
                        tinycomments_author: 'Author name',
                        });
                    </script>
                @endpush
@endsection
