@extends('layouts.master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title text-bold">Jawaban</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <a href="/jawab/create" class="btn btn-success btn-sm mb-1 text-white">Tambah</a>
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success')}}
                    </div>
                  @endif
                <table class="table table-bordered table-striped mt-3">
                  <thead>                  
                    <tr class="text-center">
                      <th></th>
                      <th style="width: 215px"></th>
                      <th style="width: 100px"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($jawab as $key => $jawab)
                        <tr>
                            <td>
                                {{$jawab->jawaban}}
                            </td>
                            <td style="display:flex">
                                <a href="/jawab/{{$jawab->id}}" class="btn btn-info">Show More</a>
                                <a href="/jawab/{{$jawab->id}}/edit" class="btn btn-success">Edit Data</a>
                            </td>
                            <td class="text-center">
                                <form action="/jawab/{{$jawab->id}}" method="post">
                                    @csrf 
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                    
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>Tidak ada jawaban</td>
                        </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            </div>
@endsection

