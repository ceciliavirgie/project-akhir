@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Jawaban</h4>
    </div>
    <div class="card-body">
        <div class="basic-form">
            <form>
                @csrf
                <div class="card-boy">
                    <input type="hidden" nama="pertanyaan_id" value="{{$jawab->pertanyaan->id}}" id="">
                    <p>{{$jawab->jawaban}}</p>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection