<?php

namespace App\Exports;

use App\Pertanyaan;
use Maatwebsite\Excel\Concerns\FromCollection;

class TanyaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Pertanyaan::all();
    }
}
