<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['user_id','pertanyaan','kategori_id'];

    public function jawab(){
        //return $this->hasMany('App\Jawab', 'jawaban', 'pertanyaan_id', 'jawab_id');
        return $this->hasMany('App\Jawab', 'pertanyaan_id');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Kategori','kategori_id');
    }
}
