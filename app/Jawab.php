<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
    protected $table = "jawaban";
    protected $fillable = ['user_id', 'pertanyaan_id', 'jawaban'];

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
