<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Jawab;
use App\Pertanyaan;

class JawabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $jawab = Jawab::all();
        // $tanya = Pertanyaan::where('id',$id)->first();
        // $tanya = Pertanyaan::all();
        return view('jawab.index', compact('jawab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jawab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $jawab = new Jawab;
        $jawab->jawaban = $request["jawaban"];
        $jawab->user_id = Auth::id();
        $jawab->pertanyaan_id = $request["temp_id"];
        $jawab->save();

        return redirect('/tanya/'.$request->temp_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jawab = Jawab::find($id);
        return view('jawab.show', compact('jawab'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jawab = Jawab::find($id);
        return view('jawab.edit', compact('jawab'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate ([
            'jawaban' => 'required'
        ]);

        $update = Jawab::where('id', $id)->update(["jawaban"=>$request["jawaban"]]);

        return redirect('/tanya/'.$request->temp_id)->with('success', 'Perubahan Data Berhasil Disimpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $jawab = Jawab::destroy($id);
        
        return redirect('/tanya/'.$request->temp_id)->with('success', 'Data Berhasil Dihapus!');
    }

    // public function jawaban(Request $request){
    //     $jawab = new Jawab;
    //     $jawab->jawaban = $request["jawaban"];
    //     $jawab->user_id = Auth::id();
    //     $jawab->temp_id = $request["temp_id"];
    //     $jawab->save();

    //     return redirect('/tanya/'.$request->temp_id);
    // }
    // $request->validate ([
        //     'jawaban' => 'required'
        // ]);

        // $jawab = Jawab::create(["jawaban"=>$request["jawaban"], 'user_id'=>Auth::user()->id, 'pertanyaan_id'=>Auth::pertanyaan()->id]);

        // return redirect('/jawab')->with('success', 'Jawaban Berhasil Disimpan!');
}
