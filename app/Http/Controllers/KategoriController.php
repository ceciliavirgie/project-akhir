<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Alert;
use DB;
use App\Kategori;
use App\Pertanyaan;


class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Kategori::all();
        
        return view('layouts.kategori.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:kategori|max:255'
        ]);

        $post = Kategori::create
        ([
            "nama" => $request["nama"]
        ]);

        Alert::success('Success', 'Berhasil menambahkan kategori');
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail=Kategori::find($id);
        return view('layouts.kategori.show', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=Kategori::find($id);
        return view('layouts.kategori.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:kategori|max:255'   
        ]);

        $update = Kategori::where('id',$id)->update(
            [
                "nama" => $request["nama"],
            ]);
        
        Alert::success('Berhasil', 'Berhasil update kategori!');
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kategori::destroy($id);
        Alert::error('Delete Kateogri', 'Berhasil menghapus kategori');
        return redirect('/kategori');
    }
}
