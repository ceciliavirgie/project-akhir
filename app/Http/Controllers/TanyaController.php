<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use App\Kategori;
use Auth;
Use Alert;

class TanyaController extends Controller
{
    public function construct(){
        $this->middleware('auth')->only(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $detail = Kategori::all();
        // $temp=Kategori::findorfail($id);
        $temp = Pertanyaan::all();
        // dd($detail);
        return view('layouts.tanya.index', compact('temp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategories = Kategori::all();
        return view('layouts.tanya.create', compact('kategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pertanyaan' => 'required|max:255'
        ]);
        $post = Pertanyaan::create([
            "pertanyaan" => $request["pertanyaan"],
            // "user_id"=> Auth::user()['id'],
            "kategori_id" => $request['category']
        ]);
        // $user = Auth::user();
        // $user->user

        // $temp=new Pertanyaan;
        // $temp->pertanyaan=$request->pertanyaan;
        // $temp->user_id=Auth::id();
        // $temp->kategori_id=$request->kategori_id;
        // $temp->save();
        // return redirect('/tanya/'.'/'.$request->kategori_id);

        toast('Pertanyaanmu Berhasil Ditambahkan!','Berhasil');
        return redirect('/tanya');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $temp=Pertanyaan::find($id);
        return view('layouts.tanya.show', compact('temp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tanya = Pertanyaan::find($id);
        return view('layouts.tanya.edit', compact('tanya'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'pertanyaan' => 'required|max:255'
        ]);
        $post = Pertanyaan::where('id',$id)->update([
            "pertanyaan" => $request["pertanyaan"],
        ]);
        Alert::success('Berhasil', 'Berhasil Update Pertanyaan!');
        return redirect('/tanya');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $temp = Pertanyaan::find($id);
        $temp->delete();
        alert()->success('Delete Pertanyaan','Berhasil Menghapus Pertanyaan');
        return redirect('/tanya');
    }
}