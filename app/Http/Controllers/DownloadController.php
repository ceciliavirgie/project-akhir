<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\TanyaExport;
use Maatwebsite\Excel\Facades\Excel;

class DownloadController extends Controller
{
    public function export() 
    {
        return Excel::download(new TanyaExport, 'Daftar Pertanyaan.xlsx');
    }
}
