<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pertanyaan;
use App\Jawab;
use App\Profile;
use DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile=Profile::where('user_id', Auth::id())->first();
        $tanya = Pertanyaan::count();
        $jawab = Jawab::count();
        $temp = Pertanyaan::all();
        // dd($detail);
        return view('layouts.profile.index', compact('temp', 'profile','tanya','jawab'));
        //return view('layouts.profile.index',compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile=Profile::where('user_id', Auth::id())->first();
        return view('layouts.profile.edit',compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate ([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $update = Profile::where('id', $id)->update(["nama"=>$request["nama"], 'umur'=>$request['umur'], 'bio'=>$request['bio'], 'alamat'=>$request['alamat']]);
        return redirect('/profile')->with('success', 'Perubahan Data Berhasil Disimpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
