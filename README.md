###PENJELASAN###

### 1. Informasi kelompok (nomor kelompok, anggota, tema project yang diangkat)
    ### Kelompok 15
    Anggota 
    1.	Fita Nova Wima Sari (@FitaNova)
    2.	Muhammad Ihsanurroihan (@mihsanurroihan)
    3.	Virgie Cecilia Johan (@Virgie)

    Tema Project : Forum Tanya-Jawab (Aplikasi web untuk Forum Pertanyaan dan jawaban dari pertanyaan)

    ### Require
    -   table "pertanyaan" terdapat data : id(int), pertanyaan(VARCHAR)
    -   table "jawaban" terdapat data : id(int), jawaban(VARCHAR)
    -   table "kategori" terdapat data : id(int), nama(VARCHAR)
    -   table "user" terdapat data : id(int), username(VARCHAR), password(VARCHAR)
    -   table "profil" terdapat data : id(int), email(VARCHAR), nama(VARCHAR), bio(TEXT), alamat(TEXT), umur(int)

    #nb : untuk ERD Diagram bisa dilihat sini 
    <link type="image/png" href="public/admin/images/erd final project.png">

    ### Description
    -   Setiap user memiliki satu profile
    -   Satu kategori dapat memiliki banyak pertanyaan.
    -   Seorang User dapat memberi jawaban dari pertayaan User lainnya. Sebuah pentayaan dapat memiliki banyak jawaban dari user yang berbeda.

### 2. Link video demo yang berisi penjelasan singkat mengenai project yang dikerjakan (template yang dipakai, demo web)
    Berikut link video demo kelompok 15
    Link demo : https://www.youtube.com/watch?v=rsi9qVhRAGA

#### 3. Link deploy di hosting sendiri atau heroku.
    Berikut link deploy hostingan kelompok 15
    Link deploy : kelompok15sanbarcode.ae-techno.com
    
    #nb mohon maaf kami tidak menggunakan .sanbercodeapp.com karena tidak mendapatkan email balasan ketika submit di hostingan (dengan status pending) sehingga kami menggunakan sub domain seadanya
